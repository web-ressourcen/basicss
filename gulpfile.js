'use strict';

var glob = require('glob'),
    gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    beeper = require('beeper'),
    colors = require('ansi-colors');

var cssdir = 'tests/assets/css';


var autoprefixerOptions = {};

/**
 * error Handler function
 * See https://github.com/mikaelbr/gulp-notify/issues/81#issuecomment-100422179
 */

var reportError = function (error) {

  var lineNumber = (error.line ? error.line : false);
  var file = (error.message ? error.message.split('\n', 1)[0] : false);

  plugins.notify({
    title: 'Task Failed [' + error.plugin + ']',
    message: (file ? file + ', ' : '') + (lineNumber ? 'Line: ' + lineNumber + ', ' : '') + 'See console for more info...',
  }).write(error);

  beeper();


  // Pretty error reporting
  var report = '';
  var chalk = colors.red;

  report += chalk('TASK:') + ' [' + error.plugin + ']\n';
  if (lineNumber) { report += chalk('LINE:') + ' ' + lineNumber + '\n'; }
  report += chalk('PROB:') + '\n' + error.message;

  console.error(report);

  // Prevent the 'watch' task from stopping
  this.emit('end');
}

gulp.task('sass:dev', async () => {
  gulp.src(cssdir + '/*.scss')
    .pipe(plugins.plumber({
      errorHandler: reportError
    }))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer(autoprefixerOptions))
    .pipe(plugins.sourcemaps.write())
    .pipe(gulp.dest(cssdir))
    .pipe(plugins.livereload())
    .on('error', reportError);
});

gulp.task('sass', async () => {
  gulp.src(cssdir + '/*.scss')
    .pipe(plugins.sass({outputStyle: 'compressed'}).on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(cssdir))
    .pipe(plugins.livereload());
});

gulp.task('watch', ()=>{
  plugins.livereload.listen();
  gulp.watch(cssdir + '/*.scss', gulp.series(['sass:dev']));
  gulp.watch('basicss/**/*.scss', gulp.series(['sass:dev']));
});

gulp.task('default',gulp.series('sass',(d)=>{d()}));
gulp.task('dev',gulp.series('default', 'watch',(d)=>{d()}));
