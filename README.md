# basicss.scss

Base styles I've been using recently...

**basicss.scss** includes [normalize.css](http://github.com/necolas/normalize.css) by Nicolas Gallagher and is heavily inspired by Harry Roberts work such as [inuitcss](http://inuitcss.com/) or his [blog](http://csswizardry.com).

## Usage:

Install dependencies:

    $ npm install
    $ yarn install

Build:

    $ gulp

Develop:

    $ gulp dev



**style.scss** should look something like this:

```
/**
 * style.scss
 *
 * Author: Cristian López
 *
 */

/**
 * basicss overrides
 *
 * any defaults in basicss/basicss/_settings.scss
 * may be overridden...
 *
 * e.g. $prototyping: true;
 */

  $fontPrimary: "Helvetica Neue", sans-serif;
  $fontSecondary: Georgia, serif;

  $colors: (
    "white": #FFFFFF,
  );

  $baseFontSize: 16px;
  $baseLineHeight: 24px;
  $baseUnit: 32px;
  $borderRadius: 4px;

  $headingSize1: 48px;
  $headingSize2: 36px;
  $headingSize3: 32px;
  $headingSize4: 24px;
  $headingSize5: 18px;
  $headingSize6: 16px;

  $headings: (
    '1': (
      'font-size': $headingSize1,
      "color": map-get($colors, "white"),
    ),
    '2': (
      'font-size': $headingSize2
    ),
    '3': (
      'font-size': $headingSize3
    ),
    '4': (
      'font-size': $headingSize4
    ),
    '5': (
      'font-size': $headingSize5
    ),
    '6': (
      'font-size': $headingSize6
    ),
  );

  $breaks: (
    tab: 481px,
    lap: 721px,
    desk: 1024px,
    desk-wide: 1200px
  );

  $constrain: (
    small:   380px,
    medium:  480px,
    large:   720px,
    max:     980px
  );


/**
 * basicss import | with gulp-sass: 'node_modules/basicss/basicss' & with sass -v2 'node_modules/basicss/basicss-react'
 */

 @import 'node_modules/basicss/basicss';

/**
 * userstyle imports
 *
 * e.g. @import 'nav';
 */
```


